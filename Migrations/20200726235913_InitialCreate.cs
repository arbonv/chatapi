﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChatUser",
                columns: table => new
                {
                    chatUserId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    userName = table.Column<string>(nullable: true),
                    passWord = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatUser", x => x.chatUserId);
                });

            migrationBuilder.CreateTable(
                name: "ChatRoom",
                columns: table => new
                {
                    chatRoomId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    chatRoomOwnerId = table.Column<int>(nullable: false),
                    roomName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatRoom", x => x.chatRoomId);
                    table.ForeignKey(
                        name: "FK_ChatRoom_ChatUser_chatRoomOwnerId",
                        column: x => x.chatRoomOwnerId,
                        principalTable: "ChatUser",
                        principalColumn: "chatUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChatMessages",
                columns: table => new
                {
                    chatMessageId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    chatUserId = table.Column<int>(nullable: true),
                    chatRoomId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatMessages", x => x.chatMessageId);
                    table.ForeignKey(
                        name: "FK_ChatMessages_ChatRoom_chatRoomId",
                        column: x => x.chatRoomId,
                        principalTable: "ChatRoom",
                        principalColumn: "chatRoomId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChatMessages_ChatUser_chatUserId",
                        column: x => x.chatUserId,
                        principalTable: "ChatUser",
                        principalColumn: "chatUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChatRoomMembers",
                columns: table => new
                {
                    chatRoomId = table.Column<int>(nullable: false),
                    chatUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatRoomMembers", x => new { x.chatRoomId, x.chatUserId });
                    table.ForeignKey(
                        name: "FK_ChatRoomMembers_ChatRoom_chatRoomId",
                        column: x => x.chatRoomId,
                        principalTable: "ChatRoom",
                        principalColumn: "chatRoomId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChatRoomMembers_ChatUser_chatUserId",
                        column: x => x.chatUserId,
                        principalTable: "ChatUser",
                        principalColumn: "chatUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_chatRoomId",
                table: "ChatMessages",
                column: "chatRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_chatUserId",
                table: "ChatMessages",
                column: "chatUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatRoom_chatRoomOwnerId",
                table: "ChatRoom",
                column: "chatRoomOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatRoomMembers_chatUserId",
                table: "ChatRoomMembers",
                column: "chatUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChatMessages");

            migrationBuilder.DropTable(
                name: "ChatRoomMembers");

            migrationBuilder.DropTable(
                name: "ChatRoom");

            migrationBuilder.DropTable(
                name: "ChatUser");
        }
    }
}
