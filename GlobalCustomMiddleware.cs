using Microsoft.AspNetCore.Builder;

namespace ChatApi
{
    public static class GlobalCustomMiddleware
    {
        /// <summary>
        /// Stolen from internet :)
        /// by having a class like this we can inject a dbcontext to middleware class
        /// </summary>
        /// <param name="app"></param>
        public static void UseGloablCustomMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<MyBasicAuthMiddleware>();
        }
    }

}

