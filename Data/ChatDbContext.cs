using ChatApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatApi.Data
{
    public class ChatDbContext : DbContext
    {
        public ChatDbContext()
        {
        }

        public ChatDbContext(DbContextOptions<ChatDbContext> optioins) : base(optioins) {

        }

        public DbSet<ChatUser> ChatUser {get; set;}

        public DbSet<ChatRoom> ChatRoom {get; set;}

        public DbSet<ChatMessages> ChatMessages {get; set;}

        public DbSet<ChatRoomMembers> ChatRoomMembers {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<ChatRoomMembers>()
            .HasKey(rm => new {rm.chatRoomId, rm.chatUserId});

            modelBuilder.Entity<ChatUser>() 
            .HasIndex(user => user.userName).IsUnique(); // do not let two users have same username

            
            modelBuilder.Entity<ChatRoomMembers>()
            .HasOne(rm => rm.ChatRoom)
            .WithMany(cr => cr.ChatRoomMembers)
            .HasForeignKey(rm => rm.chatRoomId);

            modelBuilder.Entity<ChatRoomMembers>()
            .HasOne(rm => rm.ChatUser)
            .WithMany(cu =>cu.ChatRoomMember)
            .HasForeignKey(rm => rm.chatUserId);


        }


    }
}