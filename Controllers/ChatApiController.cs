using System.Threading.Tasks;
using ChatApi.Data;
using ChatApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using ChatApi.Models.DTO;
using System;
using Microsoft.AspNetCore.Authorization;

namespace ChatApi.Controllers
{
    /// <summary>
    /// chatcontroller class exposing the api defined in the task for assigment 5
    /// </summary>
    [ApiController]
    [Route("api/Chat")]
    [Produces("application/json")]
    public class ChatApiController : ControllerBase
    {
        private readonly ChatDbContext _context;

        public ChatApiController(ChatDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Login class.!-- Let the user login to the chatroom so that the user 
        /// can store the username and password.!-- 
        /// This method doesn't need api authorization and authentication
        /// </summary>
        /// <param name="chattUserDto">Instance of ChattUserDt contains username and password </param>
        /// <returns>instance of ChatUser.!-- This instance contains the id of the chatuser and must be used
        ///  by the client api in all consecutive api calls </returns>
        [Route("~/api/Chat/Login")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<ChatUser>> LoginAsync([FromBody] ChattUserDto chattUserDto)
        {
            ChatUser user = new ChatUser { passWord = chattUserDto.Password, userName = chattUserDto.UserName };
            _context.ChatUser.Add(user);
            try
            {
                await _context.SaveChangesAsync();
                return user;

            }
            catch (Exception ex)
            {
                //trying to login more than once with same username will throw an exception
                return NotFound(ex.Message);
            }

        }


        /// <summary>
        /// cREATE ROOM API
        /// </summary>
        /// <param name="dto">RoomDto instance containg room name and id of the user who is creating this room </param>
        /// <returns></returns>
        [Route("~/api/Chat/CreateChatRoom")]
        [HttpPost]
        public async Task<ActionResult<ChatRoom>> CreateChatRoomAsync([FromBody] RoomDTO dto)
        {

            ChatRoom room = await _context.ChatRoom.Select(room => room).Where(room => room.roomName == dto.RoomName).FirstOrDefaultAsync();
            // a room with this name already exists
            if (room != null)
            {
                return NotFound("A room with this name already exists");
            }


            ChatUser chatUser = await _context.ChatUser.FindAsync(dto.UserId);
            if (chatUser == null)
            {
                return NotFound("No user for this id");
            }

            ChatRoom chatRoom = new ChatRoom()
            {
                roomName = dto.RoomName,
                ChatUserOwner = chatUser



            };
            ChatRoomMembers members = new ChatRoomMembers { ChatRoom = chatRoom, ChatUser = chatUser };
            chatUser.ChatRoomMember.Add(members);

            _context.ChatUser.Update(chatUser);

            await _context.SaveChangesAsync();

            return chatRoom;

        }

        /// <summary>
        /// Delete a chatroom 
        /// </summary>
        /// <param name="dto">RoomDton instance containg the room name and the user who want to delete this room</param>
        /// <returns>The deleted chatroom if sucess or notfound if no room exist or the user is not the owner of this room</returns>
        [Route("~/api/Chat/DeleteChatRoom")]
        [HttpDelete]
        [FormatFilter]
        public async Task<ActionResult<ChatRoom>> DeleteChatRoomAsync([FromBody] RoomDTO dto)
        {
            ChatUser chatUser = await _context.ChatUser.Include(u => u.ChatRoomMember)
            .ThenInclude(crm => crm.ChatRoom)
            .SingleAsync(u => u.chatUserId == dto.UserId);
            if (chatUser == null)
            {
                return NotFound();
            }
            var members = chatUser.ChatRoomMember.ToList();

            ChatRoom room = chatUser.ChatRoomMember
            .Select(room => room)
            .Where(room => room.ChatRoom.roomName == dto.RoomName && room.chatUserId == dto.UserId)
            .Select(s => s.ChatRoom).FirstOrDefault();

            if (room == null)
            {
                return NotFound();
            }
            //only the creator of this room should be able to delete 
            if (room.chatRoomOwnerId == dto.UserId)
            {
                _context.ChatRoom.Remove(room);
                await _context.SaveChangesAsync();
                return room;
            }
            return NotFound("You are not the creator of the room");
        }
        /// <summary>
        /// update chat room name 
        /// </summary>
        /// <param name="roomDTO">instance of UpdateRoomDTO</param>
        /// <returns>the updated room if changed, notok otherwise</returns>
        [Route("~/api/Chat/UpdateChatRoom")]
        [HttpPut]
        public async Task<ActionResult<ChatRoom>> UpdateChatRoomAsync([FromBody] UpdateRoomDTO roomDTO)
        {
            ChatUser chatUser = await _context.ChatUser
            .Include(u => u.ChatRoomMember)
            .ThenInclude(crm => crm.ChatRoom)
            .SingleAsync(u => u.chatUserId == roomDTO.UserId);
            if (chatUser == null)
            {
                return NotFound();
            }
            ChatRoom room = chatUser.ChatRoomMember.Select(r => r)
            .Where(r => r.ChatRoom.roomName == roomDTO.OldRoomName)
            .Select(s => s.ChatRoom).FirstOrDefault();
            if (room == null)
            {
                return NotFound();
            }
            if (room.chatRoomOwnerId == roomDTO.UserId)
            {
                room.roomName = roomDTO.NewRoomName;
                _context.ChatRoom.Update(room);
                await _context.SaveChangesAsync();
                return room;

            }
            return NotFound();

        }
        /// <summary>
        /// get all messages
        /// </summary>
        /// <param name="id">The room id </param>
        /// <returns>A list with all messages for this room</returns>
        [HttpGet]
        [Route("~/api/Chat/GetAllMessages")]
        public async Task<ActionResult<List<ChatMessages>>> GetAllMessagesAsync(int id)
        {

            if (id <= 0)
            {
                return NoContent();
            }

            ChatRoom chatRoom = await _context.ChatRoom.Include(u => u.ChatMessages).SingleAsync(u => u.chatRoomId == id);
            if (chatRoom != null)
            {
                return chatRoom.ChatMessages;
            }
            return NotFound("not chatroom");
        }

        [HttpGet]
        [Route("~/api/Chat/GetAllUsers")]
        /// <summary>
        /// returns the list of all users in a chatroom 
        /// </summary>
        /// <param name="id">the chatroom id</param>
        /// <returns></returns>
        public async Task<ActionResult<List<ChatUser>>> GetAllUsersAsync(int id)
        {
            if (id <= 0)
            {
                return NotFound("id is null");
            }

            ChatRoom chatRoom = await _context.ChatRoom.Include(u => u.ChatRoomMembers).SingleOrDefaultAsync(u => u.chatRoomId == id);
            if (chatRoom == null)
            {
                return NotFound("chatroom is null");

            }
            List<ChatUser> chatUsers = await _context.ChatRoomMembers.Where(i => i.ChatRoom.chatRoomId == id).Select(i => i.ChatUser).ToListAsync();
            return chatUsers;
        }

        /// <summary>
        /// Send message to a room
        /// </summary>
        /// <param name="messageDTO">messageDtO instance containing relevant information</param>
        /// <returns>ok/not ok</returns>
        [HttpPost]
        [Route("~/api/Chat/SendMessage")]
        public async Task<ActionResult> SendMessageAsync([FromBody] MessageDTO messageDTO)
        {
            ChatUser user = await _context.ChatUser.Include(u => u.ChatMessages).SingleOrDefaultAsync(u => u.chatUserId == messageDTO.UserId);
            if (user == null)
            {
                return NotFound("user not found");
            }
            ChatRoom chatRoom = await _context.ChatRoom.Include(u => u.ChatMessages).SingleOrDefaultAsync(u => u.chatRoomId == messageDTO.RoomId);
            if (chatRoom == null)
            {
                return NotFound("chat room not found");
            }
            ChatMessages chatMessages = new ChatMessages()
            {
                Message = messageDTO.ChatMessages,
                Date = DateTime.Now,
                ChatRoom = chatRoom,
                ChatUser = user
            };
            chatRoom.ChatMessages.Add(chatMessages);
            _context.Update(chatRoom);
            await _context.SaveChangesAsync();
            return Ok();
        }
        /// <summary>
        /// add (invite) a user to the chat room
        /// </summary>
        /// <param name="inviteUserDto">InviteUserDto instance containing information about
        ///   the first user id, second user id (the user who is invited or added) and room id </param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Chat/AddUserToRoom")]
        public async Task<ActionResult> AddUserToRoomAsync([FromBody] InviteUserDto inviteUserDto)
        {
            ChatUser firstUser = await _context.ChatUser
            //.AsNoTracking()
            .Include(user => user.ChatRoomMember)
            .SingleOrDefaultAsync(user => user.chatUserId == inviteUserDto.FirstUserId);
            if (firstUser == null)
            {
                return NotFound("First user not found");
            }

            ChatRoom chatRoom = await _context.ChatRoomMembers.Select(m => m).Where(s => s.chatUserId == inviteUserDto.FirstUserId).Select(s => s.ChatRoom).FirstOrDefaultAsync();

            if (chatRoom == null)
            {
                return NotFound("First user is not in the given room");
            }
            ChatUser secondUser = await _context.ChatUser.FindAsync(inviteUserDto.SecondUserId); ;

            if (secondUser == null)
            {
                return NotFound("second user not found");
            }

            ChatRoomMembers roomMembers = new ChatRoomMembers() { ChatRoom = chatRoom, ChatUser = secondUser };
            secondUser.ChatRoomMember.Add(roomMembers);
            _context.ChatUser.Update(secondUser);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                return NotFound(exception.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("~/api/Chat/GetAllRoomsForUser/")]
        /// <summary>
        /// Return the list with all rooms the user with index id in database
        /// </summary>
        /// <param name="id">The user id in the database</param>
        /// <returns>The list will rooms he is a member</returns>
        public async Task<ActionResult<List<ChatRoom>>> GetAllRoomsForUserAsync(int id)
        {
            if (id < 0)
            {
                return NotFound();
            }
            return await _context.ChatRoomMembers
            .Select(i => i)
            .Where(i => i.chatUserId == id)
            .Select(i => i.ChatRoom).ToListAsync();
        }
    }
}
