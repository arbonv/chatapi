using ChatApi.Data;
using ChatApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// You probably need to change the namespace
namespace ChatApi
{
    // Example copied from https://stackoverflow.com/questions/38977088/asp-net-core-web-api-authentication
    // Add app.UseMiddleware<MyBasicAuthMiddleware>(); to your Startup.cs file in the Configure method.
    public class MyBasicAuthMiddleware
    {
        private readonly RequestDelegate _next;

        public MyBasicAuthMiddleware(RequestDelegate next)
        {
            _next = next;

        }

        public async Task InvokeAsync(HttpContext context, ChatDbContext dbContext)
        {
            var endpoint = context.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() is object)
            {
                //This is needed for Login method in controller. The user should be able to login to chat 
                // before doing anything else. In login method we store the username and password to the database also
                await _next(context);
                return;
            }
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');

                string username = usernamePassword.Substring(0, seperatorIndex);
                string password = usernamePassword.Substring(seperatorIndex + 1);

                ChatUser user = await dbContext.ChatUser.Select(s => s)
                    .Where(r => r.passWord == password && r.userName == username)
                    .FirstOrDefaultAsync();
                if (user != null)
                {
                    await _next.Invoke(context);
                }
                else
                {
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }

            }
            else
            {
                // no authorization header
                context.Response.StatusCode = 401; //Unauthorized
                return;
            }
        }
    }
}