namespace ChatApi.Models.DTO
{
    /// <summary>
    /// Data transfer class for update room method
    /// </summary>

    public class UpdateRoomDTO
    {

        /// <summary>
        /// the old room name
        /// </summary>
        /// <value></value>
        public string OldRoomName {get; set;}

        /// <summary>
        /// The new room name
        /// </summary>
        /// <value></value>
        public string NewRoomName {get; set;}
        /// <summary>
        /// The chatt user id 
        /// </summary>
        /// <value></value>
        public int? UserId {get; set;}
    }
}