namespace ChatApi.Models.DTO
{
    /// <summary>
    /// data transfer object used in create room method
    /// </summary>
    public class RoomDTO
    {
        /// <summary>
        /// The room name
        /// </summary>
        /// <value></value>
        public string RoomName {get; set;}
        /// <summary>
        /// The user id of the user who is creating this new room
        /// </summary>
        /// <value></value>
        public int? UserId {get; set;}
    }
}