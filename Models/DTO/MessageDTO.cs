namespace ChatApi.Models.DTO
{
    /// <summary>
    /// Data transfer object class used for sending messages
    /// </summary>
    public class MessageDTO
    {
        /// <summary>
        /// The user id
        /// </summary>
        /// <value>the id of chat user</value>
       public int UserId {get; set;} 

       /// <summary>
       /// The room id 
       /// </summary>
       /// <value>room id</value>//  
       public int RoomId {get; set;}
       /// <summary>
       /// The chatt message as a string
       /// </summary>
       /// <value>the chat message</value>
       public string ChatMessages {get; set;}
    }
}