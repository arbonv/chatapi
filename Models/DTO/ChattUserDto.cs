namespace ChatApi.Models.DTO
{
    /// <summary>
    /// data transfer class used when sending parameters to Login method
    /// </summary>
    public class ChattUserDto
    {
        /// <summary>
        /// The user name
        /// </summary>
        /// <value>the users user name</value>
        public string UserName {get; set;}
        /// <summary>
        /// The chat users password
        /// </summary>
        /// <value>The password</value>
        public string Password {get; set;}
    }
}