namespace ChatApi.Models.DTO
{
    /// <summary>
    /// data transfer class used for invite user method 
    /// </summary>
    public class InviteUserDto
    {
        /// <summary>
        /// The first users id. This is the host user id
        /// </summary>
        /// <value></value>
        public int FirstUserId {get; set;}
        /// <summary>
        /// This is the guest who is invited (added) the room
        /// </summary>
        /// <value></value>
        public int SecondUserId {get; set;}
        /// <summary>
        /// The room you want to add the new user
        /// </summary>
        /// <value>the room id</value>
        public int RoomId{get; set;}
    }
}