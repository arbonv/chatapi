using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApi.Models {
    /// <summary>
    /// model class for chat messages.
    ///  a chat message is created by a user and is posted to a specific room
    /// </summary>
    public class ChatMessages {
        /// <summary>
        /// chat message id
        /// </summary>
        /// <value></value>
        [Key]
        public int chatMessageId {get; set;}
        
        /// <summary>
        /// The date the message was created
        /// </summary>
        /// <value></value>
        public DateTime Date {get; set;}
        /// <summary>
        /// The message as a string
        /// </summary>
        /// <value></value>
        public string Message {get; set;}

        
        /// <summary>
        /// Navigation property for chat user who created this message
        
        /// </summary>
        /// <value></value>
        public ChatUser ChatUser {get; set;}

        /// <summary>
        /// Navigation property for chatroom
        /// </summary>
        /// <value></value>

        public ChatRoom ChatRoom {get; set;}
        
    }
}