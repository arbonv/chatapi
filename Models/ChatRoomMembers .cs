using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ChatApi.Models {
    /// <summary>
    /// modell class describing thr join table for many to many relationship between  chatuser and chatroom 
    /// </summary>
    public class ChatRoomMembers {

        public int chatRoomId { get; set; }

        public int chatUserId {get; set;}

        /// <summary>
        /// a chatroom 
        /// </summary>
        /// <value></value>
        public ChatRoom ChatRoom {get; set;}
        /// <summary>
        /// a chatrrom can have many chatuser members
        /// </summary>
        /// <value></value>
        public ChatUser ChatUser {get; set;}
    }
}