using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatApi.Models
{
    /// <summary>
    /// Modell class describing a chatroom
    /// </summary>
    public class ChatRoom
    {   
        /// <summary>
        /// A chatroom can have many messages
        /// </summary>
        /// <typeparam name="ChatMessages"></typeparam>
        /// <returns></returns>
        private List<ChatMessages> chatMessages = new List<ChatMessages>();
        /// <summary>
        /// A chatroom can have many messages room members
        /// </summary>
        /// <typeparam name="ChatRoomMembers"></typeparam>
        /// <returns></returns>
        private List<ChatRoomMembers> chatRoomMembers = new List<ChatRoomMembers>();
        
        [Key]
        public int chatRoomId { get; set; }

        /// <summary>
        /// foreign key for chatuser owner
        /// </summary>
        /// <value></value>
        public int chatRoomOwnerId { get; set; }

        public string roomName { get; set; }
        [ForeignKey("chatRoomOwnerId")]
        public ChatUser ChatUserOwner { get; set; }
        /// <summary>
        /// a chatroom can have many messages
        /// </summary>
        /// <value></value>
        public List<ChatMessages> ChatMessages
        {
            get { return chatMessages; }
            set { chatMessages = value; }
        }

        public List<ChatRoomMembers> ChatRoomMembers
        {
            get { return chatRoomMembers; }
            set { chatRoomMembers = value; }
        }

    }
}