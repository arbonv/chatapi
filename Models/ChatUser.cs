using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ChatApi.Models
{
    /// <summary>
    /// Modell class describing a chatuser 
    /// </summary>

    public class ChatUser
    {
        private List<ChatRoomMembers> roomMembers = new List<ChatRoomMembers>();
        private List<ChatMessages> messages = new List<ChatMessages>();
        
        [Key]
        public int chatUserId { get; set; }

        public string userName { get; set; }

        public string passWord { get; set; }

        /// <summary>
        /// a chatuser can member in many rooms and a room can have many chat users 
        /// </summary>
        /// <value></value>
        public List<ChatRoomMembers> ChatRoomMember
        {
            get { return roomMembers; }
            set { roomMembers = value; }
        }
        /// <summary>
        /// List with chat messages belonging to this user
        /// </summary>
        /// <value></value>
        public List<ChatMessages> ChatMessages
        {
            get { return messages; }
            set { messages = value; }
        }

    }
}